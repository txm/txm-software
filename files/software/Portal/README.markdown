TXM portal
===

TXM portal software is a J2EE compliant web application.

Installation instructions: [https://groupes.renater.fr/wiki/txm-info/public/install_a_txm_portal](https://groupes.renater.fr/wiki/txm-info/public/install_a_txm_portal)

Current release:

* TXM_Portal_0.6.3

Older releases (obsolete):

* TXM_Portal_0.6.1beta (warning: beta level release is for experienced users testing purpose)
* TXM_Portal_0.6alpha (warning: alpha level release is not fully documented and should only be considered for development team testing purpose)
* TXM_Portal_0.5beta1 (warning: beta level release is for experienced users testing purpose)
* TXM_WEB_0.4beta1_Linux32
* TXM_WEB_0.4beta1_Linux64

